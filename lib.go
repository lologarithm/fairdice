package fairdice

import (
	"fmt"
	"math"
	"strconv"

	"gonum.org/v1/gonum/stat"
	"gonum.org/v1/gonum/stat/distuv"
)

func GetChiSq(total float64, obs []float64, numSides int) float64 {
	if numSides == 0 {
		numSides = len(obs)
	}
	exp := make([]float64, numSides)
	for i := range obs {
		exp[i] = total / float64(numSides)
	}

	// fmt.Printf("values: %v, exp: %v\n", obs, exp)
	// fmt.Printf("max: ")

	return stat.ChiSquare(obs, exp)
}

func MinRolls(obs []float64, numSides int, doAxis bool) int {
	if numSides == 0 {
		numSides = len(obs)
	}
	minRolls := numSides * 5
	if doAxis {
		minRolls *= 3
	}
	return minRolls
}

func Analyze(total float64, obs []float64, numSides int, doAxis bool) string {
	if numSides == 0 {
		numSides = len(obs)
	}

	minRolls := MinRolls(obs, numSides, doAxis)

	if total < float64(minRolls) {
		return fmt.Sprintf("%d / %d - Need more samples\n", int(total), minRolls)
	}

	csv := GetChiSq(total, obs, numSides)
	csg := distuv.ChiSquared{K: float64(numSides) - 1}
	// p := mathext.GammaIncReg(float64((-1)/2), csv/2.0)
	p := csg.CDF(csv)

	values := ""
	valCount := ""
	largest := 0.0
	for v, count := range obs {
		valStr := strconv.Itoa(v + 1)

		if len(valStr) < 2 {
			valStr = " " + valStr + " "
		} else if len(valStr) < 3 {
			valStr += " "
		}
		values += valStr

		countStr := strconv.Itoa(int(count))
		if len(countStr) < 2 {
			countStr = " " + countStr + " "
		} else if len(countStr) < 3 {
			countStr += " "
		}

		valCount += countStr
		if largest < count {
			largest = count
		}
	}
	relative := make([]int, numSides)
	scale := math.Round(math.Log2(largest) * 2)
	// fmt.Printf("Obs: %v\n", obs)
	// fmt.Printf("Scale: %0.0f\n", scale)
	for v, count := range obs {
		relative[v] = int(math.Round((count / largest) * scale))
	}
	// fmt.Printf("Relative values: %v\n", relative)

	sidesEval := ""
	pAdjust := 0.0
	// if p > 0.1 {
	// Lets try checking opposite sides
	if numSides == 6 || numSides == 8 || numSides == 12 || numSides == 20 {
		maxDiff := 0.0
		maxIndex := -1
		for i := range obs[:numSides/2] {
			opposite := numSides - i - 1
			diff := math.Abs(obs[i] - obs[opposite])
			if diff > maxDiff {
				maxDiff = diff
				if obs[opposite] > obs[i] {
					maxIndex = opposite
				} else {
					maxIndex = i
				}
			}
		}
		if doAxis {
			diffRatio := (maxDiff * float64(numSides)) / (2 * total)
			// fmt.Printf("Max axis Diff ratio: %0.3f\n", diffRatio)
			if diffRatio > 0.3 { // based on very scientific tests
				sidesEval = fmt.Sprintf("Seems weighted towards %d.\n", maxIndex+1)
			}
			pAdjust = diffRatio
		}
	}
	// }

	graph := ""

	for i := int(scale) - 1; i > 0; i-- {
		for _, relvalue := range relative {
			if relvalue >= (i + 1) {
				graph += " - "
			} else {
				graph += "   "
			}
		}
		graph += "\n"
	}

	cert := "absolutely fair"
	certain := []string{
		"certainly fair",
		"very likely fair",
		"uncertain if fair or unfair",
		"very likely unfair",
		"absolutely unfair",
	}
	certaintyValues := []float64{
		0.9,
		0.66,
		0.25,
		0.1,
		0.05,
	}

	finalP := p
	if doAxis {
		finalP += pAdjust
	}

	for i, pV := range certaintyValues {
		if (1 - finalP) > pV {
			break
		}
		cert = certain[i]
	}

	avgRoll := 0.0
	for face, count := range obs {
		avgRoll += float64(face+1) * (count / total)
	}

	finalOutput := graph + "\n" + values + "\n" + valCount + "\n" + sidesEval

	finalOutput += fmt.Sprintf("Avg Roll: %0.2f\n", avgRoll)
	finalOutput += fmt.Sprintf("P: %0.3f (%0.3f)\tDice are %s.\n\n", p, finalP, cert)

	return finalOutput
}
