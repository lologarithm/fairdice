GOOS=darwin GOARCH=arm64 go build -o ./bin/fairdice_mac_arm64.bin .
GOOS=darwin GOARCH=amd64 go build -o ./bin/fairdice_mac_amd64.bin .
GOOS=linux GOARCH=amd64 go build -o ./bin/fairdice_linux_amd64.bin .
GOOS=windows GOARCH=amd64 go build -o ./bin/fairdice_windows_amd64.exe .
echo all bins built into ./bin directory.