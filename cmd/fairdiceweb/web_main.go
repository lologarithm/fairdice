package main

import (
	_ "embed"
	"encoding/json"
	"flag"
	"io"
	"log"
	"net/http"

	"gitlab.com/lologarithm/fairdice"
)

//go:embed page.html
var pageHTML []byte

func main() {
	var addr string
	flag.StringVar(&addr, "addr", ":9321", "address to host web interface on")
	flag.Parse()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write(pageHTML)

		// htmlData, err := os.ReadFile("page.html")
		// if err != nil {

		// 	return
		// }
		// w.Write(htmlData)
	})

	http.HandleFunc("/analyze", func(w http.ResponseWriter, r *http.Request) {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("failed to read input body: " + err.Error()))
			return
		}

		req := AnalyzeRequest{}
		err = json.Unmarshal(body, &req)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("failed to read input body: " + err.Error()))
			return
		}

		total := 0.0
		obs := []float64{}
		numSides := 0
		maxSides := 100

		for _, v := range req.Rolls {
			if numSides > 0 && v > numSides {
				// fmt.Printf("Value outside of dice value range %d", numSides)
				continue
			}
			if v <= 0 || v > maxSides {
				// fmt.Printf("Invalid dice roll %d, must be between 1 and %d", v, maxSides)
				continue
			}

			if len(obs) < v {
				newObs := make([]float64, v)
				copy(newObs, obs)
				obs = newObs
			}
			obs[v-1] += 1.0
			total += 1
		}
		out := fairdice.Analyze(float64(total), obs, numSides, false)

		responseEncoded, err := json.Marshal(AnalyzeResponse{Output: out})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("failed to read input body: " + err.Error()))
			return
		}

		w.Write(responseEncoded)
	})

	log.Printf("hosting on: %s", addr)
	http.ListenAndServe(addr, nil)
}

type AnalyzeRequest struct {
	Rolls         []int
	UseAxisWeight bool
}

type AnalyzeResponse struct {
	Output string
}
