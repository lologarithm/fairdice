package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"path"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.com/lologarithm/fairdice"
)

func main() {
	var numSides, generate int
	var saveFile string
	var axisCheck, quiet, printChi bool
	flag.IntVar(&numSides, "sides", 0, "specify number of sides on the dice for better error checking")
	flag.BoolVar(&axisCheck, "checkAxis", false, "include a weighting based on each die axis on d6/8/12/20")
	flag.StringVar(&saveFile, "save", "now", "file name to save dice rolls to, by default its <current time>.dice. If file exists, its contents are injected in and allows you to resume.")
	flag.BoolVar(&quiet, "quiet", false, "Silence printing results until exiting.")
	flag.IntVar(&generate, "gen", 0, "Generate count of random dice rolls instead of inputting.")
	flag.BoolVar(&printChi, "chi", false, "only print chi sq values for graphing.")

	flag.Usage = func() {
		fmt.Print(`
Use this tool to help determine if your dice are fair.
Basic run of this tool will figure out the number of sides based on the maximum of all rolls, write all rolls to a file at the current system time.

You can pipe in the contents of these files to get the final result again, for example: 
	cat d6.dice | fairdice --save="" 
	This will read in the file, print results, and exit without creating a new save file.

If you want to add more rolls once you have a file you can either copy the contents of the file, run fairdice and then paste in, or just edit the file and then run the tool agian.

The checkAxis feature will try to add more weight to imbalanced axis (on dice that opposite faces add to #+1, like on d6/8/12/20). 
This does not use solid statistics and is my own approximation so use its output with caution.

`)
		flag.PrintDefaults()
	}
	flag.Parse()

	const maxSides = 100

	if numSides > maxSides {
		fmt.Printf("Too many sides (%d) maximum is %d", numSides, maxSides)
	}
	if generate > 0 && numSides == 0 {
		fmt.Printf("To generate dice rolls you need to input a valid number of sides.\n")
		return
	}
	input := readIn(generate, numSides, os.Stdin)
	now := time.Now()

	obs := []float64{}
	if numSides > 0 {
		obs = make([]float64, numSides)
	}
	total := 0.0

	fileData := ""
	if saveFile == "now" {
		saveFile = now.Format("2006-01-02-15-04-05") + ".dice"
	}
	dir := path.Dir(saveFile)
	if dir != "" {
		err := os.MkdirAll(dir, 0777)
		if err != nil {
			log.Fatalf("Failed to create directory to save rolls to: %s\n", err)
		}
	}

	existingFile, err := os.OpenFile(saveFile, os.O_RDONLY, 0)
	if err == nil && existingFile != nil {
		existingInput := readIn(generate, numSides, existingFile)
		go func() {
			for {
				newValue, ok := <-existingInput
				if !ok {
					existingFile.Close()
					return
				}
				input <- newValue
			}
		}()
	}

	fmt.Printf("Saving rolls to file: %s \n", saveFile)

	exit := make(chan struct{})
	monitorSigs(exit)

	for {
		select {
		case v, ok := <-input:
			if !ok {
				fmt.Print("\nFinal Results:\n\n")
				fmt.Print(fairdice.Analyze(total, obs, numSides, axisCheck))
				return
			}
			if numSides > 0 && v > numSides {
				fmt.Printf("Value outside of dice value range %d", numSides)
				continue
			}
			if v <= 0 || v > maxSides {
				fmt.Printf("Invalid dice roll %d, must be between 1 and %d", v, maxSides)
				continue
			}

			fileData += strconv.Itoa(int(v)) + "\n"

			if len(obs) < v {
				newObs := make([]float64, v)
				copy(newObs, obs)
				obs = newObs
			}
			obs[v-1] += 1.0
			total += 1

			if printChi {
				fmt.Printf("%0.2f\n", fairdice.GetChiSq(total, obs, numSides))
			} else if !quiet {
				fmt.Print(fairdice.Analyze(total, obs, numSides, axisCheck))
			}

			if saveFile != "" {
				err := os.WriteFile(saveFile, []byte(fileData), os.ModePerm)
				if err != nil {
					log.Fatalf("Failed to write to save file: %s", err)
				}
			}
		case <-exit:
			fmt.Print("\nFinal Results:\n\n")
			fmt.Print(fairdice.Analyze(total, obs, numSides, axisCheck))
			return
		}
	}
}

func monitorSigs(exit chan struct{}) {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		sig := <-sigs
		fmt.Printf("signal: %s", sig.String())
		exit <- struct{}{}
	}()
}

func readIn(generate, numSides int, input *os.File) chan int {
	output := make(chan int)

	if generate > 0 {
		go func() {
			for i := 0; i < generate; i++ {
				output <- rand.Intn(numSides) + 1
			}
			close(output)
		}()
	} else {

		reader := bufio.NewReader(input)

		go func() {
			for {
				text, err := reader.ReadString('\n')
				if err != nil {
					// log.Fatalf("error reading string: %s", err)
					// log.Printf("reading complete. closing reader now.")
					close(output)
					return
				}
				v, err := strconv.Atoi(strings.TrimSpace(text))
				if err == nil {
					output <- v
				} else {
					fmt.Printf("Failed to read input: %s\n", err)
				}
			}
		}()

	}

	return output
}
